//set main namespace
goog.provide('carpasinivores');
//get requirements
goog.require('lime.Director');
goog.require('lime.Scene');
goog.require('lime.Layer');

// entrypoint
carpasinivores.start = function(){
	//Carte
	var jeuxEc = false;
	var width = 800; //Largeur plateau
	var height =  480; //Hauteur plateau
	var vitesse_defilement = 1; //Vitesse de defilement -> Bug non fonctionnel 
	//Etre
	var tailleEtres = 20; //Taille des etres
	var vitesse = 1; //Vitesse des etres
	var vitesseR = 3; //Vitesse de rotation des etres
	var rayonOdorat = 5*tailleEtres; //Rayon de l'odorat des etres
	var vision = 15*tailleEtres; //Vision des verts
	var nbVertInitial = 10; //nb vert initial
	var nbRougeInitial = 2; //nb rouges initial
	//Eau
	var nbEauInitial = 5; //nb eau initial
	var tailleMax = 50; //Taille maximum d'une goute d'eau
	//Liste d'environnement
	var listeVerts = new Array();
	var listeRouges = new Array();
	var listeEaux = new Array();
	var etreSelect = null; //etre selectione
	//Scene director et layer
	var director = new lime.Director(document.body,width,height);
	director.makeMobileWebAppCapable();
	var gameScene = new lime.Scene();
	var lifeLayer = new lime.Layer().setPosition(width/2,height/2);
	var infoLayer = new lime.Layer().setPosition(0,0);
	//Background et label d'informations
	var background = new lime.Sprite().setSize(width,height).setFill(255,225,53);
	var infoEtreSelect = new lime.Label().setFontSize(15).setPosition(0,0).setFontColor('#fff').setOpacity(0).setFill(0,0,0,.3).setText('').setSize(0,0).setAlign('center');
	var infoEtres = new lime.Label().setFontSize(15).setPosition(0,0).setFontColor('#fff').setOpacity(1).setFill(0,0,0,.3).setText('Verts : '+nbVertInitial+' Rouges : '+nbRougeInitial).setAnchorPoint(0, 0).setSize(100,40).setAlign('center');
	
	//MenuScene//
	
	var menuScene = new lime.Scene();
	var menuLayer = new lime.Layer().setPosition(width/2,height/2);
	var menuBackground = new lime.Sprite().setSize(width,height).setFill(0,0,0).setSize(width,height);
	var boutton_play = new lime.Label().setFontSize(40).setPosition(0,-25-10).setText('Lancer').setFontColor('#000').setFill('#0f0').setSize(300,50);
	var boutton_config = new lime.Label().setFontSize(40).setPosition(0,25+10).setText('Configurer').setFontColor('#000').setFill('#0f0').setSize(300,50);
	var titre = new lime.Sprite().setSize(800,100).setPosition(0,-175).setFill('images/titre.png');
	var vertDeco = new carpasinivores.Vert(-300,150,100,width,height,0,315,9,0,0);
	goog.events.listen(boutton_play,['mousedown','touchstart'],function(e){
		this.setFill('#0c0'); 
		e.swallow(['mouseup','touchend','touchcancel'],function(){
			this.setFill('#0f0'); 
			lifeLayer.appendChild(background);
			jeuxEc = true;
			//Ajout de l'eau 
			for(i = 0;i<nbEauInitial;i++) {
		var eau = new carpasinivores.Eau(0,0,tailleMax,width,height);
		listeEaux.push(eau);
		lifeLayer.appendChild(eau);
	}
			//Ajout des etres verts
			for(i = 0;i<nbVertInitial;i++) {
				var posx = Math.floor(Math.random() * width + 1)-width/2;
				var posy = Math.floor(Math.random() * height + 1)-height/2;
				var rot = Math.floor(Math.random() * 360 + 1);
				var vert = new carpasinivores.Vert(posx,posy,tailleEtres,width,height,vitesse,rot,vitesseR,rayonOdorat,vision);
				goog.events.listen(vert,['mousedown','touchstart'],function(e){
					if(etreSelect !=this){
						if(etreSelect!=null) 
							etreSelect.setSelected(false);
						if(this instanceof carpasinivores.Vert)
							this.setFill('images/vertSelect.png');
						else if(this instanceof carpasinivores.Rouge)
							this.setFill('images/rougeSelect.png');
						if(etreSelect instanceof carpasinivores.Vert)
							etreSelect.setFill('images/vert.png');
						else if(etreSelect instanceof carpasinivores.Rouge)
							etreSelect.setFill('images/rouge.png');
						etreSelect = this;
						etreSelect.setSelected(true);
					}
					else if(etreSelect!= null && etreSelect == this){
						etreSelect.setSelected(false);
						if(etreSelect instanceof carpasinivores.Vert)
							etreSelect.setFill('images/vert.png');
						else if(etreSelect instanceof carpasinivores.Rouge)
							etreSelect.setFill('images/rouge.png');
						etreSelect = null;
					}
				});
				lifeLayer.appendChild(vert);
				listeVerts.push(vert);
			}
			//ajout des etres rouges
			for(i = 0;i<nbRougeInitial;i++) {
		var posx = Math.floor(Math.random() * width + 1)-width/2;
		var posy = Math.floor(Math.random() * height + 1)-height/2;
		var rot = Math.floor(Math.random() * 360 + 1)-360/2;
		var rouge = new carpasinivores.Rouge(posx,posy,tailleEtres,width,height,vitesse,rot,vitesseR,rayonOdorat);
		goog.events.listen(rouge,['mousedown','touchstart'],function(e){
			if(etreSelect !=this){
				if(etreSelect!=null) 
					etreSelect.setSelected(false);
				if(this instanceof carpasinivores.Vert)
					this.setFill('images/vertSelect.png');
				else if(this instanceof carpasinivores.Rouge)
					this.setFill('images/rougeSelect.png');
				if(etreSelect instanceof carpasinivores.Vert)
					etreSelect.setFill('images/vert.png');
				else if(etreSelect instanceof carpasinivores.Rouge)
					etreSelect.setFill('images/rouge.png');
				etreSelect = this;
				etreSelect.setSelected(true);
			}
			else if(etreSelect!= null && etreSelect == this){
				etreSelect.setSelected(false);
				if(etreSelect instanceof carpasinivores.Vert)
					etreSelect.setFill('images/vert.png');
				else if(etreSelect instanceof carpasinivores.Rouge)
					etreSelect.setFill('images/rouge.png');
				etreSelect = null;
			}
		});
		lifeLayer.appendChild(rouge);
		listeRouges.push(rouge);
	}  
			lifeLayer.appendChild(infoEtreSelect);
			director.replaceScene(gameScene);
		},this);
	},this);
	
	director.replaceScene(menuScene); //Par defaut le menu est affiche
	menuScene.appendChild(menuLayer);
	menuLayer.appendChild(menuBackground);
	menuLayer.appendChild(boutton_play);
	//menuLayer.appendChild(boutton_config);
	menuLayer.appendChild(titre);
	menuLayer.appendChild(vertDeco);
	//FIN : MenuScene//
	
	//GameScene//
	
	gameScene.appendChild(lifeLayer);
	gameScene.appendChild(infoLayer);	
	//Ajout du font
	lifeLayer.appendChild(background);
	infoLayer.appendChild(infoEtres);
	goog.events.listen(background,['keydown'],function(e){
		var key = e.event.event_.keyIdentifier;
		if(key=='F5' || key=='U+001B') {
			var listeVerts = new Array();
			var listeRouges = new Array();
			var listeEaux = new Array();
			var etreSelect = null;
			lifeLayer.removeAllChildren();
			location.reload();
			jeuxEc = false;
		}
		if(etreSelect == null){
			etreSelect = new carpasinivores.Etre(0,0,0,0,0,0,0,0);
		}
		if(etreSelect != null && 
		!(etreSelect instanceof carpasinivores.Vert) && 
		!(etreSelect instanceof carpasinivores.Rouge)){
			var x = etreSelect.getPosition().x;
			var y = etreSelect.getPosition().y;
			if(key=='Down') y = y + vitesse_defilement;
			else if(key=='Up') y = y - vitesse_defilement;
			else if(key=='Left') x = x - vitesse_defilement;
			else if(key=='Right') x = x + vitesse_defilement;
			etreSelect.setPosition(x,y)
		}
	},this);
	
	//FIN : GameScene//
	
	//Scheduleur de jeux //
	
	//Scheduleur affichage
	lime.scheduleManager.scheduleWithDelay(function(dt){
		infoEtres.setText('Verts : '+listeVerts.length + ' Rouges : '+listeRouges.length);
		if(etreSelect!=null)
		{
			if(etreSelect.estMort)
				infoEtreSelect.setOpacity(0);
			else 
			{
				infoEtreSelect.setOpacity(1);
				if(etreSelect instanceof carpasinivores.Vert){
					infoEtreSelect.setPosition(0,-tailleEtres-3*10);
					infoEtreSelect.setSize(128,4*20);
					infoEtreSelect.setText('Soif : '+(Math.round(etreSelect.faimSoif*1000))/1000 + 
										  ' Douleur : '+(Math.round(etreSelect.douleur*1000))/1000 + 
										  ' BébéMania : '+(Math.round(etreSelect.bebemania*1000))/1000 +
										  ' Vitesse : '+(Math.round(etreSelect.vitesse*1000))/1000);
				}
				else {
					infoEtreSelect.setPosition(0,-tailleEtres-2*10);
					infoEtreSelect.setSize(128,3*20);
					infoEtreSelect.setText('Faim : '+(Math.round(etreSelect.faimSoif*1000))/1000 + 
										   ' BébéMania : '+(Math.round(etreSelect.bebemania*1000))/1000 +
										   ' Vitesse : '+(Math.round(etreSelect.vitesse*1000))/1000);
				}
			}
		}
		else
			infoEtreSelect.setOpacity(0);
	},this,100);
	//Scheduleur de l'eau
	lime.scheduleManager.scheduleWithDelay(function(dt){
		if(jeuxEc){
			var eau = new carpasinivores.Eau(0,0,tailleMax,width,height);
			listeEaux.push(eau);
			lifeLayer.appendChild(eau,1);
			for(i=0;i<listeEaux.length;i++){
				listeEaux[i].diminue(1);
				if(listeEaux[i].getSize().width <= 0){
					lifeLayer.removeChild(listeEaux[i]);
					listeEaux[i]=listeEaux[listeEaux.length-1];
					listeEaux.pop();
				}
			}
		}
	}, this, 2000);	
	//Scheduleur des verts!
	lime.scheduleManager.schedule(function(dt){
		//mise a jours des etres verts
		for(i=0;i<listeVerts.length;i++){
			//Recherche de collisions
			var collisionV = null;
			var collisionR = null;
			var eauEnFace = null;
			var eauAutour = null;
			var eauDevant = null;
			var vert = listeVerts[i];
			for(j=0;j<listeVerts.length;j++){
				if(i!=j && vert.devant(listeVerts[j])){
					collisionV = listeVerts[j];
				}
			}
			for(j=0;j<listeRouges.length;j++){
				if(vert.devant(listeRouges[j])){
					collisionR = listeRouges[j];
				}
			}
			for(j=0;j<listeEaux.length;j++){
				if(vert.devant(listeEaux[j]) && vert.faimSoif>0){
					eauDevant = listeEaux[j];
				}
				else if(vert.objetEnFace(listeEaux[j])){
					if( eauEnFace == null) eauEnFace = listeEaux[j];
					else if(eauEnFace!=null && vert.estPlusCourt(listeEaux[j].getPosition(),eauEnFace.getPosition()))
						eauEnFace = listeEaux[j];
				}
				else if(vert.objetAutour(listeEaux[j])){
					if( eauAutour == null) eauAutour = listeEaux[j];
					else if(eauAutour!=null && vert.estPlusCourt(listeEaux[j].getPosition(),eauAutour.getPosition()))
						eauAutour = listeEaux[j];
				}
			}
			vert.updateVarVert(dt,tailleEtres);
			//Si Mort
			if(vert.estMort){
				vert.meurt();
				listeVerts[i] = listeVerts[listeVerts.length-1];
				listeVerts.pop();
			}
			//PARTIE PROGRAMMABLE PAR L'UTILISATEURS
			//Si Vivant
			/**/else{
				
				//Collision
				if(collisionV !=null) {
					//Si l'etre est assez grand
					if(collisionV.getSize().width == tailleEtres && vert.getSize().width == tailleEtres){
						
						vert.estExite(dt);
						var newVert = vert.faitUnEnfant();
						vert.bebemania = 0;
						collisionV.bebemania = 0;
						//Oh un bebe !
						if(newVert !=null) {
							goog.events.listen(newVert,['mousedown','touchstart'],function(e){
								if(etreSelect !=this){
									if(etreSelect!=null) 
										etreSelect.setSelected(false);
									if(this instanceof carpasinivores.Vert)
										this.setFill('images/vertSelect.png');
									else if(this instanceof carpasinivores.Rouge)
										this.setFill('images/rougeSelect.png');
									if(etreSelect instanceof carpasinivores.Vert)
										etreSelect.setFill('images/vert.png');
									else if(etreSelect instanceof carpasinivores.Rouge)
										etreSelect.setFill('images/rouge.png');
									etreSelect = this;
									etreSelect.setSelected(true);
								}
								else if(etreSelect!= null && etreSelect == this){
									etreSelect.setSelected(false);
									if(etreSelect instanceof carpasinivores.Vert)
										etreSelect.setFill('images/vert.png');
									else if(etreSelect instanceof carpasinivores.Rouge)
										etreSelect.setFill('images/rouge.png');
									etreSelect = null;
								}
							});
							lifeLayer.appendChild(newVert);
							listeVerts.push(newVert);
						}
					}
				}
				if(collisionV!=null) vert.genererBut();
				if(collisionR!=null) vert.genererBut();
				//Pas de Collision
				else {
					if(eauDevant!=null && vert.faimSoif>0) vert.mangeBoit(eauDevant);
					//But atteint
					else if(vert.butOk()) {
						if(vert.aFaimSoif()){
							var but = vert.plusCourteDistance(eauEnFace,eauAutour);
							if(but==null) vert.genererBut();
							else vert.setBut(but);
						}
						else vert.genererBut();
					}
					//But non atteint
					else {
						//Bien oriente
						if(vert.rotationOk()) vert.avancer(dt);
						//Mal oriente
						else vert.tourner(dt);
					}	
				}
			}
			//FIN DE LA PARTIE PROGRMMABLE PAR L'UTILISATEUR
		}
		//Mise a jour du repere
		var transX = 0;
		var transY = 0;
		var rot = 0;
		if(etreSelect!=null){
			transX = etreSelect.getPosition().x;
			transY = etreSelect.getPosition().y;
			rot = 0;
		}
		for(i=0;i<listeVerts.length;i++){
			listeVerts[i].updateRepere(transX,transY,rot);
		}
		for(i=0;i<listeRouges.length;i++){
			listeRouges[i].updateRepere(transX,transY,rot);
		}
		for(i=0;i<listeEaux.length;i++){
			listeEaux[i].updateRepere(transX,transY,rot);
		}
	},this);
	//Scheduleur des rouges !
	lime.scheduleManager.schedule(function(dt){
		for(i=0;i<listeRouges.length;i++){
			var collisionR = null;
			var collisionV = null;
			var vertAutour = null;
			var rouge = listeRouges[i];
			//Collision avec un rouge
			for(j=0;j<listeRouges.length;j++){
				if(i!=j && rouge.devant(listeRouges[j])){
					collisionR = listeRouges[j];
				}
			}
			for(j=0;j<listeVerts.length;j++){
				//Un vert autour
				if(rouge.objetAutour(listeVerts[j])){
					if(vertAutour == null) vertAutour = listeVerts[j];
					else if(vertAutour != null && rouge.estPlusCourt(listeVerts[j].getPosition(),vertAutour.getPosition())) 
						vertAutour = listeVerts[j];
				}
				//Un vert devant
				if(rouge.devant(listeVerts[j])){
					collisionV = listeVerts[j];
				}
			}
			rouge.updateVarRouge(dt,tailleEtres);
			//Si rouge mort
			if(rouge.estMort){
				rouge.meurt();
				listeRouges[i] = listeRouges[listeRouges.length-1];
				listeRouges.pop();
			}
			//Si Vivant
			else{
				//Collision avec un rouge
				if(collisionR !=null) {
					//Si l'etre est assez grand
					if(collisionR.getSize().width == tailleEtres && rouge.getSize().width == tailleEtres){
						rouge.estExite(dt);
						var newRouge = rouge.faitUnEnfant();
						rouge.bebemania = 0;
						collisionR.bebemania = 0;
						//Oh un bebe !
						if(newRouge !=null) {
							goog.events.listen(newRouge,['mousedown','touchstart'],function(e){
								if(etreSelect !=this){
									if(etreSelect!=null) 
										etreSelect.setSelected(false);
									if(this instanceof carpasinivores.Vert)
										this.setFill('images/vertSelect.png');
									else if(this instanceof carpasinivores.Rouge)
										this.setFill('images/rougeSelect.png');
									if(etreSelect instanceof carpasinivores.Vert)
										etreSelect.setFill('images/vert.png');
									else if(etreSelect instanceof carpasinivores.Rouge)
										etreSelect.setFill('images/rouge.png');
									etreSelect = this;
									etreSelect.setSelected(true);
								}
								else if(etreSelect!= null && etreSelect == this){
									etreSelect.setSelected(false);
									if(etreSelect instanceof carpasinivores.Vert)
										etreSelect.setFill('images/vert.png');
									else if(etreSelect instanceof carpasinivores.Rouge)
										etreSelect.setFill('images/rouge.png');
									etreSelect = null;
								}
							});
							lifeLayer.appendChild(newRouge);
							listeRouges.push(newRouge);
						}
					}
					rouge.genererBut();
				}
				//collision avec un vert
				if(collisionV !=null) {
					//Si le rouge a faim
					if(rouge.faimSoif>0){
						collisionV.aMal();
						rouge.mangeBoit(collisionV);
						if(collisionV.douleur<1) rouge.setBut(rouge.getPosition());
						else rouge.genererBut();
					}
					else rouge.genererBut();
				}
				//Pas de collision avec un vert ni un rouge et butOK
				else if(rouge.butOk() && !collisionV && !collisionR) {
					//Si il y a des vert autour
					if(vertAutour != null && rouge.aFaimSoif())	rouge.setBut(vertAutour.but);
					//Si rien autour ou pas faim
					else rouge.genererBut();
				}
				//Rouge n'a pas atteint son but
				else if(rouge.rotationOk()) rouge.avancer(dt);
				//Rouge n'est pas dans la bonne direction
				else rouge.tourner(dt);	
			}
		}
		//Mise a jour du repere
		var transX = 0;
		var transY = 0;
		var rot = 0;
		if(etreSelect!=null){
			transX = etreSelect.getPosition().x;
			transY = etreSelect.getPosition().y;
			rot = 0;
		}
		for(i=0;i<listeVerts.length;i++){
			listeVerts[i].updateRepere(transX,transY,rot);
		}
		for(i=0;i<listeRouges.length;i++){
			listeRouges[i].updateRepere(transX,transY,rot);
		}
		for(i=0;i<listeEaux.length;i++){
			listeEaux[i].updateRepere(transX,transY,rot);
		}
	},this);	
	
	//FIN : Scheduleur de jeux //
}


//this is required for outside access after code is compiled in ADVANCED_COMPILATIONS mode
goog.exportSymbol('carpasinivores.start', carpasinivores.start);
