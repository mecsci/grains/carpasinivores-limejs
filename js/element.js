//Namespace
goog.provide('carpasinivores.Element');

//Require
goog.require('lime.Sprite');

//Constructeur
carpasinivores.Element = function(x,y,taille,width,height){
	goog.base(this);
	this.setSize(taille,taille).setPosition(x,y);
	this.width = width;
	this.height = height;
}
	
goog.inherits(carpasinivores.Element,lime.Sprite);

//Mise a jour du repere
carpasinivores.Element.prototype.updateRepere = function(transX,transY,rotation){
	//Rotation
	//Translation
	var x = this.getPosition().x-transX;
	var y = this.getPosition().y-transY;
	//Recadrage pos
	if(x > this.width/2) x = -this.width + x;
	if(x < - (this.width/2)) x = this.width -x;
	if(y > this.height/2) y = -this.height + y;
	if(y < -(this.height/2)) y = this.height - y;	
	this.setPosition(x,y);
}

//Retourne la position de l'elelement le plus proche
carpasinivores.Element.prototype.plusCourteDistance = function(a,b){
	if(a!=null || b!=null){
		if(a==null) return b.getPosition();
		else if(b==null) return a.getPosition();
		else if(this.estPlusCourt(a,b)) return a.getPosition();
		else return b.getPosition();
	}
	return null;
}

//Retourne l'element le plus proche
carpasinivores.Element.prototype.estPlusCourt = function(a,b){
	var dist1 = this.distance(this.getPosition(),a);
	var dist2 = this.distance(this.getPosition(),b);
	return (dist1<dist2);
}

//Distance entre l'element et l'objet
carpasinivores.Element.prototype.distance = function(objet){
	return goog.math.Coordinate.distance(this.getPosition(),objet);
}