//Namespace
goog.provide('carpasinivores.Eau');

//Require
goog.require('carpasinivores.Element');

//Constructeur
carpasinivores.Eau = function(x,y,tailleMax,width,height){
	goog.base(this,x,y,taille,width,height);
	var taille = Math.floor(Math.random() * tailleMax + 1);
	this.setSize(taille,taille);
	var x = Math.floor(Math.random() * this.width + 1)-this.width/2;
	var y = Math.floor(Math.random() * this.height + 1)-this.height/2;
	this.setPosition(x,y);
	this.width = width;
	this.height = height;
	this.setFill('images/eau.png');
}

goog.inherits(carpasinivores.Eau,carpasinivores.Element);

//Diminution du diametre de l'eau de X px
carpasinivores.Eau.prototype.diminue = function(x){
	var taille = this.getSize().width;
	this.setSize(taille-x,taille-x);
}
	

