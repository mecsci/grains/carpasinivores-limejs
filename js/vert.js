//Namespace
goog.provide('carpasinivores.Vert');

//Require
goog.require('carpasinivores.Etre');

//Constructeur
carpasinivores.Vert = function(x,y,taille,width,height,vitesse,rotation,vitesseR,rayonOdorat,vision){
	goog.base(this,x,y,taille,width,height,vitesse,rotation,vitesseR,rayonOdorat,vision);
	this.setFill('images/vert.png');
	this.vision = vision;
	this.douleur = 0;
	this.fatigue = 0;
}

goog.inherits(carpasinivores.Vert,carpasinivores.Etre);

//fait un enfant avec des chance de : 
//1/2 pour une bebemania > 0.9
//2/5 pour une bebemania >0.7
//3/10 pour une bebemania >0.5
//1/5 pour une bebemania >0.3
//1/10 pour une bebemania > 0.1
//0 pour une bebemania < 0.1
carpasinivores.Vert.prototype.faitUnEnfant = function(){
	var chance = 0;
	if(this.bebemania<0.1) chance = 0;
	else if(this.bebemania<0.3) chance = 1;
	else if(this.bebemania<0.5) chance = 2;
	else if(this.bebemania<0.7) chance = 3;
	else if(this.bebemania<0.9) chance = 4;
	else chance = 5;
	var random = Math.floor(Math.random() * 10 + 1);
	if(random<=chance) {
		this.bebemania = 0;
		return new carpasinivores.Vert(this.getPosition().x,this.getPosition().y,this.getSize().width/2,this.width,this.height,this.vitesseMax,this.getRotation(),this.vitesseR,this.rayonOdorat,this.vision);
	}
	else return null;
}

//Met � jour la douleur
carpasinivores.Vert.prototype.aMal = function(){
	this.douleur = this.douleur+0.01;
	this.bebemania = this.bebemania-0.01;
}

//TODO : faire marcher l'h�ritage !
//Met a jour les variables de l'etre vert
carpasinivores.Vert.prototype.updateVarVert = function(dt,tailleVert){
	//Gestion de la vitesse
	if(this.faimSoif>0.5 && this.vitesse>0.5) {
		this.vitesse = this.vitesse - 0.0001 * dt/15;
		if(this.vitesse < 0.5) this.vitesse = 0.5;
	}
	if(this.faimSoif<0.5 && this.vitesse<this.vitesseMax) {
		this.vitesse = this.vitesse + 0.0001 * dt/15;
		if(this.vitesse > this.vitesseMax) this.vitesse = this.vitesseMax;
	}
	//Gestion de la soif
	if(this.faimSoif<1){
		if(this.soif>0.5)this.bebemania = this.bebemania - 0.01;
		this.faimSoif = this.faimSoif + 0.0001 * dt/15;
		if(this.faimSoif>=1) this.estMort = true;
	}
	//Gestion de la douleur
	if(this.douleur >0) {
		this.douleur = this.douleur - 0.0001 * dt/15;
		if(this.douleur < 0) this.douleur = this.douleur = 0;
	}
	if(this.douleur >= 1) this.estMort = true;
	//Gestion de la bebemania
	if(this.bebemania<1 && this.faimSoif<0.5 && this.douleur<0.5) this.bebemania = this.bebemania + 0.0001 * dt/15;
	if(this.douleur > 0.5 && this.faimSoif > 0.5) this.bebemania = this.bebemania - 0.1 * dt/15;
	if(this.bebemania<0) this.bebemania = 0;
	if(this.bebemania>1) this.bebemania = 1;
	//Met � jour la taille
	var taille = this.getSize().width;
	if(taille < tailleVert){
		this.setSize(taille+0.003*dt/15,taille+0.003*dt/15);
		if(taille > tailleVert) this.setSize(tailleVert,tailleVert);
	}
}

//Determine si l'objet est en face et a port� d'odorat
carpasinivores.Etre.prototype.objetEnFace = function(objet){
	var angle = this.getRotation() * Math.PI / 180;
	//Un point de la droite
	var ax = this.getPosition().x + (Math.sin(angle) * -10); 
	var ay = this.getPosition().y + (Math.cos(angle) * -10); 
	//Un autre point de la droite
	var bx = this.getPosition().x;
	var by = this.getPosition().y;
	//Centre du certcle
	var cx = objet.getPosition().x;
	var cy = objet.getPosition().y;
	//Rayon du cercle
	var p = (objet.getSize().width)/2;
	
	var a = Math.pow(bx-ax,2) + Math.pow(by-ay,2);
	var b = 2*((bx-ax)*(ax-cx) + (by-ay)*(ay-cy))
	var c = Math.pow(ax,2) + Math.pow(ay,2) + Math.pow(cx,2) + Math.pow(cy,2) - 2*(ax*cx+ay*cy) - Math.pow(p,2);
	
	var res = Math.pow(b,2) - 4 * a * c;
	
	var dist = this.distance(objet);
	
	return (res>=0 && dist<=this.vision);		
}