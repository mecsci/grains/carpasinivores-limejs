//Namespace
goog.provide('carpasinivores.Etre');

//Require
goog.require('carpasinivores.Element');
goog.require('lime.animation.FadeTo');

//Constructeur
carpasinivores.Etre = function(x,y,taille,width,height,vitesse,rotation,vitesseR,rayonOdorat){
	goog.base(this,x,y,taille,width,height);
	this.setRotation(rotation);
	this.but = new goog.math.Coordinate(x,y);
	this.vitesseMax = vitesse; //Vitesse maximum de deplacement
	this.vitesse = vitesse; //Vitesse de deplacement
	this.vitesseR = vitesseR; //Vitesse de rotation
	this.angle = this.choixAngle;
	this.selected = false; 
	this.estMort = false; 
	this.rayonOdorat  = rayonOdorat; 
	this.faimSoif = 0;
	this.bebemania = 0.5;
}
	
goog.inherits(carpasinivores.Etre,carpasinivores.Element);

//Donne x et y comme but a l'etre
carpasinivores.Etre.prototype.setBut = function(b){
	this.but.x = b.x;
	this.but.y = b.y;
	this.choixAngle();
}

//Fait mourir l'etre
carpasinivores.Etre.prototype.meurt = function(){
	var fade = new lime.animation.FadeTo(0).setDuration(1);
	this.runAction(fade);
	this.estMort = true;
}

//TODO : faire marcher l'h�ritage et la redefinition de methode !
//Met a jour le repere de l'etre
carpasinivores.Etre.prototype.updateRepere = function(transX,transY,rotation){
	//goog.base(this,updateRepere,transX,transY,rotation);
	//Rotation
	//Translation
	var x = this.getPosition().x-transX;
	var y = this.getPosition().y-transY;
	//Recadrage pos
	if(x > this.width/2) x = -this.width + x;
	if(x < - (this.width/2)) x = this.width -x;
	if(y > this.height/2) y = -this.height + y;
	if(y < -(this.height/2)) y = this.height - y;	
	this.setPosition(x,y);
	var butX = this.but.x-transX;
	var butY = this.but.y-transY;
	//Recadrage but
	if(butX > this.width/2) butX = -this.width + butX;
	if(butX < - (this.width/2)) butX = this.width -butX;
	if(butY > this.height/2) butY = -this.height + butY;
	if(butY < -(this.height/2)) butY = this.height - butY;
	
	this.but.x = butX;
	this.but.y = butY;
}

//Setter de l'attribut selected
carpasinivores.Etre.prototype.setSelected = function(selected){
	this.selected = selected;
}

//Retourne vrai si l'angle de l'etre est bon, faux sinon
carpasinivores.Etre.prototype.rotationOk = function(){
	return this.getRotation() == this.angle;
}

//Renvoi vrai si le but est atteint, faux sinon
carpasinivores.Etre.prototype.butOk = function(){
	return goog.math.Coordinate.equals(this.getPosition(), this.but);
}

//Retourne vrai si un etre se trouve devant, faux sinon
//TODO : qqe bugs � corriger...
carpasinivores.Etre.prototype.devant = function(etre2){
	var distance = goog.math.Coordinate.distance(this.getPosition(),etre2.getPosition());
	distance -= etre2.getSize().width/2;
	distance -= this.getSize().width/2;
	if(distance <= 0)
	{
		var rot = this.calculeAngle(this.but.x,this.but.y);
		var rot2 = this.calculeAngle(etre2.getPosition().x,etre2.getPosition().y);
		if(rot2 <= rot+90 && rot2 >= rot-90) return true;
		else return false;
	}
	else return false;
}

//Genere un nouveau but � l'etre al�atoirement
carpasinivores.Etre.prototype.genererBut = function(){
	this.but.x = Math.floor(Math.random() * this.width + 1)-this.width/2;
	this.but.y = Math.floor(Math.random() * this.height + 1)-this.height/2;
	this.choixAngle();
}

//Met a jour la bebeMania
carpasinivores.Etre.prototype.estExite = function(dt){
	this.bebemania = this.bebemania+0.1*dt/15;
}

//TODO : Choisi de partir a gauche (1) ou a droite (-1)
carpasinivores.Etre.prototype.gaucheDroite = function(){
	if(this.angle > (360 - this.angle)) return -1
	else return 1;
}

//Calcule de l'angle que doit obtenir l'�tre pour atteindre un but
carpasinivores.Etre.prototype.calculeAngle = function(x,y){	
	var angle = Math.atan2(y-this.getPosition().y, x-this.getPosition().x)*180/Math.PI;
	angle = ((angle - 270)%360)*-1;
	if(angle<0) angle = (angle + 360)%360;
	return angle;
}

//Choix de l'angle � prendre pour atteindre le but le plus rapidement possible
carpasinivores.Etre.prototype.choixAngle = function(){
	var angle;
	var but2x;
	var but2y;
	var petitCoteX = this.width/2 - Math.abs(this.but.x);
	petitCoteX += this.width/2 - Math.abs(this.getPosition().x);
	var petitCoteY = this.height/2 - Math.abs(this.but.y);
	petitCoteY += this.height/2 - Math.abs(this.getPosition().y);
	if(this.getPosition().x < this.but.x)
		but2x = this.getPosition().x - petitCoteX;
	else	
		but2x = this.getPosition().x +petitCoteX;
	if(this.getPosition().y < this.but.y)
		but2y = this.getPosition().y - petitCoteY;
	else	
		but2y = this.getPosition().y + petitCoteY;
	//Le d�calage est sur le y
	if(Math.abs(this.but.y) > Math.abs(this.but.x)){
		but2x = this.but.x;
	}
	//Le decalage est sur les x
	else if(Math.abs(this.but.y) < Math.abs(this.but.x)){
		but2y = this.but.y;
	}
	d1 = goog.math.Coordinate.distance(this.getPosition(),this.but);
	d2 = goog.math.Coordinate.distance(this.getPosition(),new goog.math.Coordinate(but2x,but2y));
	//Attribution de l'angle seulon la distance la plus courte
	if(d1 <= d2) this.angle = this.calculeAngle(this.but.x,this.but.y);
	else this.angle = this.calculeAngle(but2x,but2y);
}

//Fait avancer l'etre
carpasinivores.Etre.prototype.avancer = function(dt){
	//Calcule de la direction et de l'avancement
	var angle = this.getRotation() * Math.PI / 180;
	var x = this.getPosition().x + (Math.sin(angle) * -this.vitesse * dt/15); 
	var y = this.getPosition().y + (Math.cos(angle) * -this.vitesse * dt/15); 
	//Gestion de l'atteinte du but celon la vitesse
	if(x >= this.but.x-this.vitesse * dt/15 && x <= this.but.x+this.vitesse * dt/15) x = this.but.x;
	if(y >= this.but.y-this.vitesse * dt/15 && y <= this.but.y+this.vitesse * dt/15) y = this.but.y;
	this.setPosition(x,y);
}
	
//Fait tourner l'etre vers son but
carpasinivores.Etre.prototype.tourner = function(dt){
	//Partir a gauche ou a droite
	var gd = this.gaucheDroite();
	//Mise � jours de l'angle
	if(this.getRotation() >= this.angle -this.vitesseR * dt/15 && this.getRotation() <= this.angle + this.vitesseR * dt/15) 
		this.setRotation(this.angle); 
	else
		this.setRotation((this.getRotation() + gd * this.vitesseR * dt/15)); 
	//Pas de n�gatifs !
	if(this.getRotation() < 0) this.setRotation(359);
}

//Definit si l'etre a faim/soif
carpasinivores.Etre.prototype.aFaimSoif = function(){
	return (this.faimSoif>0.3);
}

//Fait boir l'etre
carpasinivores.Etre.prototype.mangeBoit = function(objet){
	this.faimSoif = this.faimSoif - 0.01;
	if(this.faimSoif<0) this.faimSoif = 0;
	if(objet instanceof carpasinivores.Eau) objet.diminue(2);
	if(objet instanceof carpasinivores.Vert) objet.aMal();
}

//Determine si l'objet est en face
carpasinivores.Etre.prototype.objetEnFace = function(objet){
	var angle = this.getRotation() * Math.PI / 180;
	//Un point de la droite
	var ax = this.getPosition().x + (Math.sin(angle) * -10); 
	var ay = this.getPosition().y + (Math.cos(angle) * -10); 
	//Un autre point de la droite
	var bx = this.getPosition().x;
	var by = this.getPosition().y;
	//Centre du certcle
	var cx = objet.getPosition().x;
	var cy = objet.getPosition().y;
	//Rayon du cercle
	var p = (objet.getSize().width)/2;
	
	var a = Math.pow(bx-ax,2) + Math.pow(by-ay,2);
	var b = 2*((bx-ax)*(ax-cx) + (by-ay)*(ay-cy))
	var c = Math.pow(ax,2) + Math.pow(ay,2) + Math.pow(cx,2) + Math.pow(cy,2) - 2*(ax*cx+ay*cy) - Math.pow(p,2);
	
	var res = Math.pow(b,2) - 4 * a * c;
	
	return (res>=0);		
}

//Determine si l'objet ce trouve � porter d'odorat de letre
carpasinivores.Etre.prototype.objetAutour = function(objet){
	var dist = goog.math.Coordinate.distance(objet.getPosition(),this.getPosition());
	dist = dist - this.getSize().width/2 - (objet.getSize().width)/2;
	return (dist<=this.rayonOdorat);
}