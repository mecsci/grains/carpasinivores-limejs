//Namespace
goog.provide('carpasinivores.Rouge');

//Require
goog.require('carpasinivores.Etre');

//Constructeur
carpasinivores.Rouge = function(x,y,taille,width,height,vitesse,rotation,vitesseR,rayonOdorat){
	goog.base(this,x,y,taille,width,height,vitesse,rotation,vitesseR,rayonOdorat);
	this.setFill('images/rouge.png');
}

goog.inherits(carpasinivores.Rouge,carpasinivores.Etre);

//TODO : faire marcher l'h�ritage et la redefinition de methode !
//Met a jour les variables de l'etre
carpasinivores.Rouge.prototype.updateVarRouge = function(dt,tailleRouge){
	//Gestion de la vitesse
	if(this.faimSoif>0.5 && this.vitesse>0.5) {
		this.vitesse = this.vitesse - 0.0001 * dt/15;
		if(this.vitesse < 0.5) this.vitesse = 0.5;
	}
	if(this.faimSoif<0.5 && this.vitesse<this.vitesseMax) {
		this.vitesse = this.vitesse + 0.0001 * dt/15;
		if(this.vitesse > this.vitesseMax) this.vitesse = this.vitesseMax;
	}
	//Gestion de la soif
	if(this.faimSoif<1){
		if(this.soif>0.5)this.bebemania = this.bebemania - 0.01;
		this.faimSoif = this.faimSoif + 0.0001 * dt/15;
		if(this.faimSoif>=1) this.estMort = true;
	}
	//Gestion de la bebemania
	if(this.faimSoif<0.5) this.bebemania = this.bebemania + 0.0001 * dt/15;
	if(this.faimSoif > 0.5) this.bebemania = this.bebemania - 0.1 * dt/15;
	if(this.bebemania<0) this.bebemania = 0;
	if(this.bebemania>1) this.bebemania = 1;
	//Met � jour la taille
	var taille = this.getSize().width;
	if(taille < tailleRouge){
		this.setSize(taille+0.005*dt/15,taille+0.005*dt/15);
		if(taille > tailleRouge) this.setSize(tailleRouge,tailleRouge);
	}
}

//Creer un nouvel etre rouge
carpasinivores.Rouge.prototype.faitUnEnfant = function(){
	var chance = 0;
	if(this.bebemania<0.1) chance = 0;
	else if(this.bebemania<0.3) chance = 1;
	else if(this.bebemania<0.5) chance = 2;
	else if(this.bebemania<0.7) chance = 3;
	else if(this.bebemania<0.9) chance = 4;
	else chance = 5;
	var random = Math.floor(Math.random() * 10 + 1);
	if(random<=chance) {
		this.bebemania = 0;
		return new carpasinivores.Rouge(this.getPosition().x,this.getPosition().y,this.getSize().width/2,this.width,this.height,this.vitesseMax,this.getRotation(),this.vitesseR,this.rayonOdorat,this.vision);
	}
	else return null;
}